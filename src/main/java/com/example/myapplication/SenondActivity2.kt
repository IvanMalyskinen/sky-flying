package com.example.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.google.android.gms.auth.api.Auth
import java.security.AuthProvider
import java.util.*

lateinit var provider : List<AuthUI.IdpConfig>
private val REQUEST_CODE: Int = 7117
class SenondActivity2 : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_senond2)

        provider = Arrays.asList<AuthUI.IdpConfig>(
            AuthUI.IdpConfig.EmailBuilder().build() // Email login
        )
        showSignInOptions()
    }
    private fun showSignInOptions() {
        val My_REQUEST_CODE = 0
        startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
    .setAvailableProviders(provider)
    .setTheme(R.style.MyTheme)
    .build(), My_REQUEST_CODE)
    }
}